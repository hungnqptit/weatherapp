import React from 'react';
import {createStore, applyMiddleware} from 'redux';
import {Provider} from 'react-redux';
import createSagaMiddleware from 'redux-saga';
import searchReducer from '../reducers/search';
import WeatherApp from './weatherApp';
import rootSaga from '../sagas/rootSaga';

const sagaMiddleware = createSagaMiddleware();
const store = createStore(searchReducer, applyMiddleware(sagaMiddleware));
sagaMiddleware.run(rootSaga);
const Main = () => (
  <Provider store={store}>
    <WeatherApp />
  </Provider>
);

export default Main;
