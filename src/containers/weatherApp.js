import React, {Component} from 'react';
import {View} from 'react-native';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import WeatherInfo from '../components/weatherInfo';
import SearchBox from '../components/searchBox';
import Options from '../components/option';
import styles from '../styles';
import * as weatherActions from '../actions';
import Geolocation from '@react-native-community/geolocation';
import * as ART from '@react-native-community/art';

export class WeatherApp extends Component {
  // componentDidMount() {
  //   Geolocation.getCurrentPosition(
  //     position => {
  //       const lat = position.coords.latitude.toString();
  //       const lon = position.coords.longitude.toString();
  //       this.props.actions.searchByCoordinates(lat, lon);
  //     },
  //     () => {
  //       const errorMessage = 'Could not fetch weather for your location';
  //       this.props.actions.setErrorMessage(errorMessage);
  //     },
  //     {enableHighAccuracy: true, timeout: 20000, maximumAge: 1000},
  //   );
  // }

  componentDidMount() {
    this.props.actions.searchByCoordinates('20.9780671', '105.79381839999999');
  }

  render() {
    const {state, actions} = this.props;
    const {weatherData, errorMessage, isLoading, isFahrenheit} = state;
    //this.props.actions.searchByCoordinates(20.9780671, 105.79381839999999);
    return (
      <View style={styles.container}>
        <SearchBox onComplete={actions.searchByCity} />
        <WeatherInfo
          weatherData={weatherData}
          errorMessage={errorMessage}
          isLoading={isLoading}
          isFahrenheit={isFahrenheit}
        />
        <Options
          style={styles.options}
          onValueChange={value => actions.setIsFahrenheit(value)}
          isFahrenheit={isFahrenheit}
        />
      </View>
    );
  }
}

export default connect(
  state => ({
    state,
  }),
  dispatch => ({
    actions: bindActionCreators(weatherActions, dispatch),
  }),
)(WeatherApp);
