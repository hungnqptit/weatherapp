import {delay} from 'redux-saga';
import {all} from 'redux-saga/effects';
import {watchSearchByCity, watchSearchByCoordinates} from './weatherSagas';

export default function* rootSaga() {
  yield all([watchSearchByCity(), watchSearchByCoordinates()]);
}
