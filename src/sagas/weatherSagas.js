import delay from 'redux-saga';
import {put, takeEvery} from 'redux-saga/effects';
import config from '../../config';
import {
  UPDATE_SEARCH_TERM,
  UPDATE_WEATHER_DATA,
  SET_ERROR_MESSAGE,
  SET_IS_LOADING,
  SET_IS_FAHRENHEIT,
  setErrorMessage,
  setIsLoading,
  updateWeatherData,
} from '../actions';

function* searchByCity(searchTerm) {
  try {
    const {appid, url} = config;
    console.log('search By City');
    yield put({type: 'SET_IS_LOADING', isLoading: true});
    let response;
    yield (response = fetch(`${url}?q=${searchTerm}&appid=${appid}`));
    yield (response = response.toJSON());
    put({type: 'SET_ERROR_MESSAGE', errorMessage: ''});
    put({type: 'SET_IS_LOADING', isLoading: false});
    put({type: 'UPDATE_WEATHER_DATA', weatherData: response});
    console.log(response);
  } catch (error) {
    put({type: UPDATE_WEATHER_DATA, weatherData: {}});
    put({
      type: 'SET_ERROR_MESSAGE',
      errorMessage: `Could not fetch weather for ${searchTerm}`,
    });
  }
}

export function* watchSearchByCity() {
  yield takeEvery('SEARCH_BY_CITY', searchByCity);
  console.log('Watch Search By City');
}

function* searchByCoordinates() {
  console.log('searchByCoordinatesSaga');
  const {appid, url} = config;
  put(setIsLoading(true));
  console.log('searchByCoordinatesSaga');
  try {
    console.log(
      fetch(`${url}?lat=20.9780671&lon=105.79381839999999&appid=${appid}`),
    );
    let response;
    yield (response = fetch(
      `${url}?lat=20.9780671&lon=105.79381839999999&appid=${appid}`,
    ));
    yield (response = response.json());
    put({type: 'SET_ERROR_MESSAGE', errorMessage: ''});
    put({type: 'SET_IS_LOADING', isLoading: false});
    put({type: UPDATE_WEATHER_DATA, weatherData: response});
    console.log(response);
  } catch (error) {
    put({type: UPDATE_WEATHER_DATA, weatherData: {}});
    put({
      type: 'SET_ERROR_MESSAGE',
      errorMessage: 'Could not fetch weather for this location',
    });
  }
}

export function* watchSearchByCoordinates() {
  yield takeEvery('SEARCH_BY_COORDINATES', searchByCoordinates);
  console.log('watchSearchByCoordinates');
}
