export const UPDATE_SEARCH_TERM = 'UPDATE_SEARCH_TERM';
export const UPDATE_WEATHER_DATA = 'UPDATE_WEATHER_DATA';
export const SET_ERROR_MESSAGE = 'SET_ERROR_MESSAGE';
export const SET_IS_LOADING = 'SET_IS_LOADING';
export const SET_IS_FAHRENHEIT = 'SET_IS_FAHRENHEIT';
export const SEARCH_BY_CITY = 'SEARCH_BY_CITY';
export const SEARCH_BY_COORDINATES = 'SEARCH_BY_COORDINATES';

export function updateSearchTerm(searchTerm) {
  return {
    type: UPDATE_SEARCH_TERM,
    searchTerm,
  };
}

export function updateWeatherData(weatherData) {
  return {
    type: UPDATE_WEATHER_DATA,
    weatherData,
  };
}

export function setErrorMessage(errorMessage) {
  return {
    type: SET_ERROR_MESSAGE,
    errorMessage,
  };
}

export function setIsLoading(isLoading) {
  return {
    type: SET_IS_LOADING,
    isLoading,
  };
}

export function setIsFahrenheit(isFahrenheit) {
  return {
    type: SET_IS_FAHRENHEIT,
    isFahrenheit,
  };
}

export function searchByCity(searchTerm) {
  return {
    type: SEARCH_BY_CITY,
    searchTerm,
  };
}

export function searchByCoordinates(latitude, longtitude) {
  return {
    type: SEARCH_BY_COORDINATES,
    latitude,
    longtitude,
  };
}
